package model_loader;

import net.coobird.thumbnailator.Thumbnails;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ImageConverter {

    public static BufferedImage convert(BufferedImage inputImage, int desiredWidth, int desiredHeight, int desiredChannels) throws IOException{
        //	convert to greyscale
        inputImage = desiredChannels < 2 ?
                new BufferedImage(inputImage.getWidth(), inputImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY)
                : inputImage;
        Graphics g = inputImage.getGraphics();
        g.drawImage(inputImage, 0, 0, null);
        g.dispose();

        return Thumbnails.of(inputImage)
                .size(desiredWidth, desiredHeight)
                .keepAspectRatio(false)
                .asBufferedImage();
    }

}

package model_loader;

import javafx.application.Application;
import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by agata on 07.12.17.
 */


public final class PretrainedClassifier {
    private static int height = 0, width = 0, channels = 1;
    private static int batchSize = 30;

    public static void main(String[] args) throws IOException, PictureDimensionsMismatchException{

        String[] argumentsToDialog = new String[]{};
        Application.launch(FileChooserDialog.class, argumentsToDialog);
        String pictureFilePath = FileChooserDialog.getPictureFilePath();
        String[] picturesFilesPaths = FileChooserDialog.getPicturesFilesPaths();

        String modelFilePath = FileChooserDialog.getModelFilePath();
        String metadataFilePath = FileChooserDialog.getMetadataFilePath();

        if (Objects.isNull(pictureFilePath)) {
            if (Objects.isNull(picturesFilesPaths)) {
                return;
            }
        } else {
            if( Objects.isNull(picturesFilesPaths)){
                picturesFilesPaths = new String[]{pictureFilePath};
            }
        }

        System.out.println("files: " + Arrays.toString(picturesFilesPaths));

        String[] labels = {};
        try(BufferedReader br = new BufferedReader(new FileReader(metadataFilePath))){
            String[] values = br.readLine().split("\\s*,\\s*");
            width = Integer.valueOf(values[0]);
            height = Integer.valueOf(values[1]);
            channels = Integer.valueOf(values[2]);
            labels = br.lines().map(String::trim).filter(s -> s.length() > 0).toArray(String[]::new) ;
        } catch(IOException e){
            System.err.println("Improperly formatted metadata file");
            return;
        }


        System.out.println("expected width: " + width);
        System.out.println("expected height: " + height);

        MultiLayerNetwork loadedNetwork;
        if (Objects.isNull(modelFilePath)) {
            System.out.println("modelFilePath is null");

            return;
        } else {
            System.out.println("file: " + modelFilePath.toString());
            try {
                loadedNetwork = ModelSerializer.restoreMultiLayerNetwork(Paths.get(modelFilePath).toFile());
            } catch (IOException e) {
                System.out.println("Couldn't restore the model");
                return;
            }
        }
        //System.out.println(loadedNetwork.params().toString());
        //System.out.println(loadedNetwork.getLayerWiseConfigurations().toString());


        NativeImageLoader imageLoader = new NativeImageLoader(height, width, channels);
        //get image into INDARRAY

        for( String filePathStr : picturesFilesPaths ) {
            File file = new File(filePathStr);

            BufferedImage sourceImage;
            try {
                sourceImage = ImageIO.read(file);
            } catch(IOException e){
                System.err.println("Couldn't read a file");
                return;
            }

            // Throw Error if you don't want to scale to proper dimensions
            /*
            int actualWidth = sourceImage.getWidth();
            int actualHeight = sourceImage.getHeight();
            if(actualWidth != width || actualHeight != height) {
                throw new PictureDimensionsMismatchException();
            }
            */
            BufferedImage converted;
            try{
                converted = ImageConverter.convert(sourceImage, width, height, channels);
            } catch(IOException e){
               System.err.println("Couldn't convert an image to desired shape");
               return;
            }

            imageLoader.asMatrix(converted);
            INDArray image = imageLoader.asMatrix(file); //throws IOException
            DataNormalization scaler = new ImagePreProcessingScaler(0, 1); //normalize 0-256 greyscale values into 0-1 values
            scaler.transform(image);
            INDArray output = loadedNetwork.output(image);

            //System.out.println("Results: ");
            //System.out.println("file chosen: " + filePathStr );
            Path filePath = Paths.get(filePathStr);
            System.out.println("File directory: " + filePath.getParent().getFileName().toString() + ", filename: " + filePath.getFileName() );
            String[] probabilities = output.toString().replaceAll("[\\[\\]]","").split("\\s*,\\s*");
            int topN = 5;
            LinkedHashMap<String, Double> sortedScores = sortDescByValueThenByKey( convertToScoreMap(labels, probabilities), topN );
            /*sortedScores.keySet()
                    .stream()
                    .limit(topN)
                    .collect(Collectors.toMap(key -> key,
                            key -> (sortedScores.get(key)),
                            (e1, e2) -> e1,
                            LinkedHashMap::new));
            */
           /* sortedScores = sortedScores.entrySet()
                    .stream()
                    .limit(topN)
                    .collect(Collectors.toMap(entry -> entry.getKey(),
                            entry -> entry.getValue() ));

*/
            //System.out.println("Probabilities " + Arrays.toString(probabilities) );
            //System.out.println("Labels: " + Arrays.toString(labels));

            System.out.println("\t" + sortedScores.toString());




        }



/*

        ImageRecordReader testRecordReader = new ImageRecordReader(height, width, channels, labelMaker);
        testRecordReader.initialize();
        DataSetIterator testDataIter = new RecordReaderDataSetIterator(testRecordReader, batchSize);
        scaler.fit(testDataIter);
        testDataIter.setPreProcessor(scaler);

        Evaluation evalTest = loadedNetwork.evaluate(testDataIter, testDataIter.getLabels(), topN);//top-N accuracy

        while(testDataIter.hasNext()){
            DataSet next = testDataIter.next();
            INDArray output = loadedNetwork.output(next.getFeatureMatrix());
            evalTest.eval(next.getLabels(),output);
        }

        */




        /*
        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        File mainPath = new File(System.getProperty("user.dir"), "src/main/resources/faces/");
        FileSplit fileSplit = new FileSplit(mainPath, NativeImageLoader.ALLOWED_FORMATS, rng);
        //BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, batchSize);
        //BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, numExamples); //last was batchSize, but it makes with training only with this first batch
        BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, numExamples);// Math.min(numExamples, batchSize*40)); //last was batchSize, but it makes with training only with this first batch
        //System.out.println(pathFilter.toString());

        InputSplit[] inputSplit = new FileSplit(); //fileSplit.sample(pathFilter, splitTrainTest, 1 - splitTrainTest); // training and testing set division according to proportions (splitTrainTest)
        InputSplit trainData = inputSplit[0];
        InputSplit testData = inputSplit[1];
        System.out.println("train size: " + trainData.length());
        System.out.println("test size: " + testData.length());

        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);



        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);
        DataSetIterator trainDataIter;
        ImageRecordReader testRecordReader = new ImageRecordReader(height, width, channels, labelMaker);
        DataSetIterator testDataIter;
        trainDataIter = new RecordReaderDataSetIterator(recordReader, batchSize, (int)testData.length() , numLabels);
        scaler.fit(trainDataIter);
        trainDataIter.setPreProcessor(scaler);
        DataSet testDataSet = trainDataIter.next();
        ModelSerializer.restoreMultiLayerNetwork().predict();
*/



        /*  recordReader.initialize(testData);
        //trainDataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels);
        trainDataIter = new RecordReaderDataSetIterator(recordReader, batchSize, (int)testData.length() , numLabels);

        scaler.fit(trainDataIter);
        trainDataIter.setPreProcessor(scaler);
        DataSet testDataSet = trainDataIter.next();
        ModelSerializer.restoreMultiLayerNetwork().predict();*/

//        System.out.println(loadedNetwork.predict());

            /*
        //Save the model
        File locationToSave = new File("MyMultiLayerNetwork.zip");      //Where to save the network. Note: the file is in .zip format - can be opened externally
        boolean saveUpdater = true;                                             //Updater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this if you want to train your network more in the future
        ModelSerializer.writeModel(net, locationToSave, saveUpdater);

        //Load the model
        MultiLayerNetwork restored = ModelSerializer.restoreMultiLayerNetwork(locationToSave);

        System.out.println("Saved and loaded parameters are equal:      " + net.params().equals(restored.params()));
        System.out.println("Saved and loaded configurations are equal:  " + net.getLayerWiseConfigurations().equals(restored.getLayerWiseConfigurations()));
        */


    }


    private static Map<String, Double> convertToScoreMap(String[] labels, String[] probabilities){
        Map<String, Double> result = new HashMap<>();
        Iterator<String> labelsIterator = Arrays.asList(labels).iterator();
        Iterator<String> probabilitiesIterator = Arrays.asList(probabilities).iterator();
        while( labelsIterator.hasNext() && probabilitiesIterator.hasNext() ){
            result.put(labelsIterator.next(), Double.valueOf(probabilitiesIterator.next()));
        }
        return result;
    }




/*

    public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortDescByValue(Map<K, V> map) {

        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return -(o1.getValue()).compareTo( o2.getValue() );
            }
        });

        LinkedHashMap<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
*/

/*
    public static LinkedHashMap<String, Double> sortDescByValueThenByKey(Map<String, Double> map, int topN) {
        return map.entrySet().stream()
                .sorted(Map.Entry.<String,Double>comparingByValue()
                        .thenComparing(Map.Entry.comparingByKey()))
                .limit(topN)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new)
                );
    }
    */

    public static <K extends Comparable, V extends Comparable<? super V>> LinkedHashMap<K, V> sortDescByValueThenByKey(Map<K, V> map, int topN) {
        return (LinkedHashMap)map.entrySet().stream()
                .sorted(Map.Entry.<K,V>comparingByValue().reversed().thenComparing(Map.Entry.comparingByKey()))
                .limit(topN)
                .collect(Collectors.toMap(
                        Map.Entry<K,V>::getKey,
                        Map.Entry<K,V>::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap<K,V>::new)
                );
    }

}


package model_loader;


import java.awt.Desktop;
import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public final class FileChooserDialog extends Application {
    private static File modelFile = null;
    private static File pictureFile = null;
    private static List<File> picturesFileList = null;
    private static File metadataFile = null;
    private Desktop desktop = Desktop.getDesktop();
    private static FileChooser fileChooser;
    private static Button openModelButton;
    private static Button openMetadataButton;
    private static Button openImageButton;
    private static Button openMultipleImagesButton;
    private static Button okButton;
    private static String pathStringToImages =  System.getProperty("user.home") + "/Downloads/IntelliJ/IdeaProjects/CNN_FaceRecognition_Thesis/src/main/resources/faces";
    private static String[] extensionsOfImages =  {"*.png", "*.jpg"};
    private static String extensionsOfImagesDescriptor =  "Image files (*.png, *.jpg)";
    private static String pathStringToModels =  System.getProperty("user.home") + "/Downloads/IntelliJ/IdeaProjects/CNN_FaceRecognition_Thesis/charts_and_data";
    private static String[] extensionsOfModels =  {"*.bin", "*.zip"};
    private static String extensionsOfModelsDescriptor =  "Binary files (*.bin) or (*.zip)";
    private static String pathStringToMetadata =  pathStringToModels; //System.getProperty("user.home") + "/Downloads/IntelliJ/IdeaProjects/CNN_FaceRecognition_Thesis/src/main/resources/faces";
    private static String[] extensionsOfMetadata =  {"*.txt"};
    private static String extensionsOfMetadataDescriptor =  "TXT files (*.txt)";

    private static String defaultPathString =  System.getProperty("user.dir");


    public static String getModelFilePath(){
        return modelFile == null ? null : modelFile.getAbsolutePath();
    }

    public static String getMetadataFilePath(){
        return metadataFile == null ? null : metadataFile.getAbsolutePath();
    }

    public static String getPictureFilePath(){
        return pictureFile ==null ? null : pictureFile.getAbsolutePath();
    }

    public static String[] getPicturesFilesPaths(){
        return picturesFileList == null ? null : picturesFileList.stream().map(File::getAbsolutePath).toArray(String[]::new);
    }

    private static boolean isModelChosen(){
        return !Objects.isNull(modelFile);
    }

    private static boolean isMetadataChosen(){
        return !Objects.isNull(metadataFile);
    }

    private static boolean isPictureChosen(){
        return !Objects.isNull(pictureFile) || !Objects.isNull(picturesFileList);
    }
    private static boolean isAllChosen(){
        System.out.println("isModelChosen? " + isModelChosen());
        System.out.println("isPictureChosen? " + isPictureChosen());
        return isModelChosen() && isMetadataChosen() && isPictureChosen();
    }

    private static void checkIfReady(Button clicked){
        System.out.println(clicked.getId());
        //clicked.getStyleClass().removeAll();
        clicked.getStyleClass().add("chosen");
        if( isAllChosen() ){
            //stage.close();
            okButton.getStyleClass().removeAll("notAllChosen");
            okButton.getStyleClass().add("allChosen");
        }

    }

    private static void checkConfirm(Button confirmButton){
        if(!isAllChosen()) {
            confirmButton.getStyleClass().removeAll();
            confirmButton.getStyleClass().add("notAllChosen");
        }
    }

    private void handleFileChoice(Button btnSrc, Supplier<Object> action, String pathString, String defaultPathString, String extensionsDescription, String[] extensions){
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter(extensionsDescription, extensions);
        fileChooser.getExtensionFilters().removeAll();
        fileChooser.getExtensionFilters().add(extensionFilter);
        File userDirectory = new File(pathString);
        if(!userDirectory.canRead()){
            userDirectory = new File(defaultPathString);
        }
        fileChooser.setInitialDirectory(userDirectory);
        Object element = action.get();
        if(element != null){
            checkIfReady(btnSrc);
        }
    }

    @Override
    public void start(final Stage stage) {
        fileChooser = new FileChooser();
        openModelButton = new Button("Choose a model...");
        openMetadataButton = new Button("Choose a metadata file...");

        openImageButton = new Button("Choose a picture...");
        openMultipleImagesButton = new Button("Choose pictures...");
        okButton = new Button("Confirm");
        stage.setTitle("CNN Face Recognition");

        openModelButton.setOnAction( (e) -> {
            Supplier<Object> action = () -> modelFile = fileChooser.showOpenDialog(stage);
            handleFileChoice((Button)e.getSource(), action, pathStringToModels, defaultPathString, extensionsOfModelsDescriptor, extensionsOfModels);
        });
        openMetadataButton.setOnAction( (e) -> {
            Supplier<Object> action = () -> metadataFile = fileChooser.showOpenDialog(stage);
            handleFileChoice((Button)e.getSource(), action, pathStringToMetadata, defaultPathString, extensionsOfMetadataDescriptor, extensionsOfMetadata);
        });

        openImageButton.setOnAction( (e) -> {
            Supplier<Object> action = () -> pictureFile = fileChooser.showOpenDialog(stage);
            handleFileChoice((Button)e.getSource(), action, pathStringToImages, defaultPathString, extensionsOfImagesDescriptor, extensionsOfImages);
            /*FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter(extensionsOfImagesDescriptor, extensionsOfImages);
            fileChooser.getExtensionFilters().removeAll();
            fileChooser.getExtensionFilters().add(extensionFilter);
            fileChooser.setInitialDirectory(new File(pathStringToImages));
            pictureFile = fileChooser.showOpenDialog(stage);
            if(pictureFile != null){
                checkIfReady((Button)e.getSource());
            }*/
        } );
        openMultipleImagesButton.setOnAction( (e)-> {
            Supplier<Object> action = () -> picturesFileList = fileChooser.showOpenMultipleDialog(stage);
            handleFileChoice((Button)e.getSource(), action, pathStringToImages, defaultPathString, extensionsOfImagesDescriptor, extensionsOfImages);
            /*
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter(extensionsOfImagesDescriptor, extensionsOfImages);
            fileChooser.getExtensionFilters().removeAll();
            fileChooser.getExtensionFilters().add(extensionFilter);
            fileChooser.setInitialDirectory(new File(pathStringToImages));
            picturesFileList = fileChooser.showOpenMultipleDialog(stage);
            if(picturesFileList != null) {
                checkIfReady((Button) e.getSource());
            }
            */
        });
        okButton.setOnAction( (e)-> {
            checkConfirm((Button)e.getSource());
            if(isAllChosen()){
                stage.close();
            }

        });



        final GridPane inputGridPane = new GridPane();

        GridPane.setConstraints(openModelButton, 0, 0);
        GridPane.setConstraints(openMetadataButton, 0, 2);
        GridPane.setConstraints(openImageButton, 8, 0);
        GridPane.setConstraints(openMultipleImagesButton, 8, 2);
        GridPane.setConstraints(okButton, 4, 4);
        inputGridPane.setHgap(50);
        inputGridPane.setVgap(50);
        inputGridPane.getChildren().addAll(openModelButton, openMetadataButton, openImageButton, openMultipleImagesButton, okButton);

        final Pane rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(20, 20, 20, 20));

        Scene scene = new Scene(rootGroup);
        stage.setScene(scene);

        File f = new File("src/model_loader/styles.css");
        scene.getStylesheets().clear();
        scene.getStylesheets().add("file:///" + f.getAbsolutePath().replace("\\", "/"));

        stage.show();


    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
